
<!-- I didn't use Javascript here since the content is static for now. 
If there would be a CMS backing these posts I would create the tiles dynamically
with JS.  -->
<div class='explore-list'>
    <div class="facility-post">
        <img class="facility-post-image" src='/assets/deck-inn.jpg' alt='2for1'/>
        <p class="facility-post-title">The Deck Restaurant</p>
    </div>

    <div class="facility-post">
        <img class="facility-post-image" src='/assets/tap-inn.jpg' alt='beer'/>
        <p class="facility-post-title">Tap Inn Bar</p>
    </div>
</div>