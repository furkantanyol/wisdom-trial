<div class='header header-initial'>
    <img src='/assets/logo.svg' alt='logo' class='logo'/>
    <div class='menu-wrapper'>
        <img src='/assets/icon-search.svg' class='menu-item'/>
        <img src='/assets/icon-menu.svg' class='menu-item'/>
    </div>
</div>