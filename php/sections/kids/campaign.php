<div class='kids-section-text-wrapper'>
    <div class='kids-section-text'>
        <p class='top-text'>kids eat</p>
        <p class='mid-text'>free<span class='asterisk'>*</span></p>
        <p class='bottom-text'><span class='asterisk'>*</span>Kids under 6 yrs old, Sundays after 4pm</p>
    </div>
    <button class="explore-post-button kids-button">EXPLORE</button>
</div>