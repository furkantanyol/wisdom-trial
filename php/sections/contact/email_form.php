<?php
    // SET SQL VARIABLES
    $servername = "db";
    $username = "devuser";
    $password = "devpass";
    $dbname = "test_db";

    // CREATE SQL CONNECTION
    $conn = new mysqli($servername, $username, $password, $dbname);
    $conn->set_charset("utf8");

    // CHECK SQL CONNECTION
    if ($conn->connect_error) {
        http_response_code(500);
        die("Connection failed: " . $conn->connect_error);
    } 

    // PROCESS POST REQUESTS
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // GET FORM FIELDS & FIX WHITESPACE
        $firstName = strip_tags(trim($_POST["firstName"]));
        $firstName = str_replace(array("\r","\n"),array(" "," "),$firstName);
        $lastName = strip_tags(trim($_POST["lastName"]));
        $lastName = str_replace(array("\r","\n"),array(" "," "),$lastName);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        
        // VALIDATE FIELDS
        if ( empty($firstName) OR empty($lastName) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        // CHECK IF CONTACT EXISTS IN DB
        $stmt = $conn->prepare("SELECT email FROM contacts WHERE email = ?");
        $stmt->bind_param('s', $email);
        $stmt->execute();
        $result = $stmt->get_result();
        if(mysqli_num_rows($result) > 0){
            http_response_code(400);
            echo "Record already exists";
            exit;
        }else{
            // BUILD EMAIL CONTENT
            $recipient = "support@wisdom.com.au";
            $subject = "New contact from $name";

            $email_content = "First Name: $firstName\n";
            $email_content .= "Last Name: $lastName\n";
            $email_content .= "Email: $email\n\n";

            $email_headers = "From: $name <$email>";

            // SEND EMAIL
            if (mail($recipient, $subject, $email_content, $email_headers)) {

                // PREVENT SQL INJECTION USING STATEMENT INSTEAD OF QUERY
                // ADD CONTACT TO CONTACTS DB
                $stmt = $conn->prepare("INSERT INTO contacts (firstname, lastname, email) VALUES (?, ?, ?)");
                $stmt->bind_param('sss', $firstName, $lastName, $email);
                $stmt->execute();
                $result = $stmt->get_result();
                if($stmt->errno !== 0) { 
                    http_response_code(500);
                    $conn->close();
                    echo "Update failed: " . $stmt->error; 
                    exit;
                }else{

                    http_response_code(200);
                    $conn->close();
                    echo "Thank You! Your message has been sent.";
                    exit;
                }

            } else {
                // Set a 500 (internal server error) response code.
                http_response_code(500);
                echo "Oops! Something went wrong and we couldn't send your message.";
            }
        }
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }
?>