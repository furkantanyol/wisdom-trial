<div class='menu'>
    <ul class='menu-list'>
        <li class='menu-title'><a class='link'>Functions</a></li>
        <li class='menu-el'><a class='link'>Corporate</a></li>
        <li class='menu-el'><a class='link'>Weddings</a></li>
        <li class='menu-el'><a class='link'>Parties</a></li>
        <li class='menu-el'><a class='link'>Functions</a></li>
    </ul>
    <ul class='menu-list'>
        <li class='menu-title'><a class='link'>Facilities</a></li>
        <li class='menu-el'><a class='link'>Top Deck Restaurant</a></li>
        <li class='menu-el'><a class='link'>Ocean Front Bar</a></li>
        <li class='menu-el'><a class='link'>Murphy McEwan</a></li>
        <li class='menu-el'><a class='link'>Lounge</a></li>
    </ul>
    <ul class='menu-list'>
        <li class='menu-title'><a class='link'>Accomodation</a></li>
        <li class='menu-el'><a class='link'>Reservations</a></li>
        <li class='menu-el'><a class='link'>Rooms</a></li>
        <li class='menu-el'><a class='link'>Packages</a></li>
        <li class='menu-el'><a class='link'>Attractions</a></li>
    </ul>
</div>