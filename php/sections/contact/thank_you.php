<html>
    <head>
       <link rel = "stylesheet" type = "text/css" href = "thank_you.css" />
    </head>
    <body>
        <div class="thank-you-header">
            <img src="/assets/logo.svg" alt="logo" class="logo">
        </div>
        <div class="thank-you-content">
                <p class="thank-you-text">Thank you for reaching us!  <br/> We received your contact details and will be in contact shortly.</p>
            <a class="back-button" href="http://localhost:8000">GO BACK</a>
        </div>
        <div class="footer">
            <p class="copyright-text">The Nineteenth 2018 - All Rights Reserved. Site by Wisdom.</p>
            <div class="footer-link-wrapper">
                <a class="footer-link">Terms</a>
               <a class="footer-link">Partners</a>
               <a class="footer-link">Contact</a>
            </div>
        </div>
    </body>
</html>