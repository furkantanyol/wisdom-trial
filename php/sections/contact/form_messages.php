
    <p class='form-message-success'>Thank You! Your message has been sent.</p>
    <p class='form-message-error'>There was a problem with your submission, please try again.</p>
    <p class='form-message-not-valid'>Please enter valid credentials and try again.</p>