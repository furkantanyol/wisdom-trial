

<div class='form-wrapper'>
    <div class='contact-us'>
        <p class='contact-us-title'>Contact Us</p>
    </div>
    <form class='form' id='form' method="post" action="sections/contact/email_form.php">  
       <div class='inputs-wrapper'>
            <div class='input-element-wrapper'>
                <p class='input-label'>*First Name<span class="error"> <?php echo $firstNameErr;?></span></p>
                <input class='input' type="text" name="firstName" value="<?php echo $firstName;?>">
                <p class='input-small-label'>First Name</p>
            </div>
            <div class='input-element-wrapper'>
                <p class='input-label'><span class='last-name'>Last name</span><span class="error"> <?php echo $lastNameErr;?></span></p>
                <input class='input' type="text" name="lastName" value="<?php echo $lastName;?>">
                <p class='input-small-label'>Last Name</p>
            </div>
            
            <div class='input-element-wrapper email-input-wrapper'>
                <p class='input-label'>*Email Address <span class="error"> <?php echo $emailErr;?></span></p>
                <input class='input'  type="text" name="email" value="<?php echo $email;?>">                
            </div>
       </div>
       
        <button class='submit-button' type="submit"> SEND </button>  
    </form>
    <?php include("sections/contact/form_messages.php"); ?>


</div>