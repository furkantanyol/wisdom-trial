
<!-- I didn't use Javascript here since the content is static for now. 
If there would be a CMS backing these posts I would create the tiles dynamically
with JS.  -->
<div class='explore-list'>
    <div class="explore-post">
        <img class="explore-post-image" src='/assets/tile-1.jpg' alt='2for1'/>
        <div class="explore-text-wrapper">
            <p class="explore-post-title">2 for 1 Dinners</p>
            <p class="explore-post-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales lectus id velit.</p>
            <button class="explore-post-button">EXPLORE</button>
        </div> 
    </div>

    <div class="explore-post">
        <img class="explore-post-image" src='/assets/tile-2.jpg' alt='beer'/>
        <div class="explore-text-wrapper">
            <p class="explore-post-title">$4 Beer Tuesdays</p>
            <p class="explore-post-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales lectus id velit.</p>
            <button class="explore-post-button">EXPLORE</button>
        </div> 
    </div>

    <div class="explore-post">
        <img class="explore-post-image" src='/assets/tile-3.jpg' alt='bingo'/>
        <div class="explore-text-wrapper">
            <p class="explore-post-title">Wednesday Bingo</p>
            <p class="explore-post-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales lectus id velit.</p>
            <button class="explore-post-button">EXPLORE</button>
        </div> 
    </div>

    <div class="explore-post">
        <img class="explore-post-image" src='/assets/tile-4.jpg' alt='pizza'/>
        <div class="explore-text-wrapper">
            <p class="explore-post-title">TGI Friday Pizzas</p>
            <p class="explore-post-excerpt">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sodales lectus id velit.</p>
            <button class="explore-post-button">EXPLORE</button>
        </div> 
    </div>
</div>