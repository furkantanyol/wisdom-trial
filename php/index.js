const animatedElements = [
  'header',
  'blue-triangle',
  'blue-triangle-mid',
  'yellow-triangle',
  'orange-triangle'
]

const form = document.getElementsByClassName('form')[0]
const formMessageSuccess = document.getElementsByClassName(
  'form-message-success'
)[0]
const formMessageError = document.getElementsByClassName(
  'form-message-error'
)[0]
const formMessageNotValid = document.getElementsByClassName(
  'form-message-not-valid'
)[0]

window.addEventListener(
  'scroll',
  throttle(() => {
    animatedElements.forEach(elem => toggleActiveClass(elem))
  }),
  1000
)

form.addEventListener('submit', e => handleFormSubmit(e))

async function handleFormSubmit(e) {
  e.preventDefault()
  const formData = new FormData(form)
  const response = await fetch(form.getAttribute('action'), {
    method: 'POST',
    body: formData
  })

  switch (response.status) {
    case 200:
      showMessage(formMessageSuccess)
      break
    case 400:
      showMessage(formMessageNotValid)
      break
    default:
      showMessage(formMessageError)
  }
  form.reset()
}

function toggleActiveClass(className) {
  const classDiv = document.getElementsByClassName(className)[0]

  if (isScrolledIntoView(classDiv) === true) {
    classDiv.className = `${className} ${className}-active`
  } else {
    classDiv.className = className
  }
}

function isScrolledIntoView(elem) {
  const docViewTop = window.scrollY
  const docViewBottom = docViewTop + window.innerHeight

  const elemTop = elem.className.includes('header')
    ? elem.offsetTop
    : getPageTop(elem)
  const elemBottom = elemTop + elem.offsetHeight

  return elemBottom <= docViewBottom && elemTop >= docViewTop
}

function getPageTop(el) {
  const rect = el.getBoundingClientRect()
  const docEl = document.documentElement
  return rect.top + (window.pageYOffset || docEl.scrollTop || 0)
}

function throttle(fn, delay) {
  let lastCall = 0
  return function(...args) {
    const now = new Date().getTime()
    if (now - lastCall < delay) {
      return
    }
    lastCall = now
    return fn(...args)
  }
}

function showMessage(messageElement) {
  const initialClassname = messageElement.className
  messageElement.className += ` form-message-active`
  setTimeout(() => {
    messageElement.className = initialClassname
  }, 5000)
}
