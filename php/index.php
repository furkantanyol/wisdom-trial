<!-- ./php/index.php -->

<html>
  <head>
    <title>Hello World</title>
    <link rel = "stylesheet" type = "text/css" href = "index.css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,500&display=swap" rel="stylesheet">
    <link href="/assets/fonts/Mort-Modern-Medium.ttf" rel="stylesheet">
    <script  src="index.js" defer></script>

  </head>

  <body>
      <div class='header-section'>
        <?php include("sections/header/header.php"); ?>
        <?php include("sections/header/triangles.php"); ?>
      </div>
      <div class='explore-section'>
        <?php include("sections/explore/venue-description.php"); ?>
        <?php include("sections/explore/posts.php"); ?>
      </div>
      <div class='kids-section'>
        <div class='skewed-bg'></div>
        <?php include("sections/kids/triangles.php"); ?>
        <?php include("sections/kids/campaign.php"); ?>
      </div>
      <div class='facilities-section'>
        <?php include("sections/facilities/description.php"); ?>
        <?php include("sections/facilities/posts.php"); ?>
      </div>
      <div class='contact-section'>
        <?php include("sections/contact/menu.php"); ?>
        <?php include("sections/contact/form.php"); ?>
      </div>
      <div class='section'></div>
      <div class='section'></div>


  </body>
</html>
