WISDOM THE NINETEENTH WEBSITE DEMO
===================================

USED TECHNOLOGIES
-------------------
- php
- vanilla javascript
- html
- css

INSTALLATION
--------------

- make sure docker is installed in your computer ([Download Docker for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac))
- clone or copy the project to a directory
- go to that directory in terminal and execute the following command (This may take a while)
  `docker-compose build && docker-compose up`
- from your browser go to http://localhost:8000

TESTING
-------
EMAIL
-----
I set the email config to be Mailtrap in `ssmtp.conf` file. You can change the config for Wisdom later.

- Enter values to the contact form and send the form.
- To test the emails sent go to the following link ([Mailtrap io](https://mailtrap.io/signin))

Email: furkantanyol@gmail.com
Pass: WisdomEmail123

- Click demo inbox after logging in and you can see the emails there.

DATABASE
--------

- go to project directory in terminal and execute the following command:
  `docker ps`

- the docker images will be shown. then check the image name for mysql and run `docker exec -ti wisdom-trial_db_1 sh`

- inside the bash login to db by `mysql -u devuser -p`. It will ask for the password. Enter `devpass`.

- You should be in mysql now.

- Go to our db `USE test_db;`
- Search for the data in our contacts table `SELECT * FROM contacts;`
- You should see the contact that you entered.

Please let me know if you have any questions.
